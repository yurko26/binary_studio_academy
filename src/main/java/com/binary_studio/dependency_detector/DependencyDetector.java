package com.binary_studio.dependency_detector;

import java.util.ArrayList;
import java.util.List;

public final class DependencyDetector {

    private DependencyDetector() {
    }

    public static boolean canBuild(DependencyList libraries) {
        List<String> library = libraries.libraries;
        List<String[]> dependencies = libraries.dependencies;
        if(library.isEmpty() && dependencies.isEmpty()) return true;
        for (String[] dependency : dependencies) {
            for (String s : dependency) {
                if (!library.contains(s)) {
                    return false;
                }
            }
        }
        List<String> firstLibrary = new ArrayList<>();
        for (String[] strings : dependencies) {
            for (int j = 0; j < strings.length; j++) {
                firstLibrary.add(strings[0]);
            }
        }
        for (String[] strings : dependencies) {
            for (int j = 0; j < strings.length; j++) {
                if(firstLibrary.contains(strings[1])) return false;
            }
        }
        return true;
    }
}
