package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {
	private String name;
	private PositiveInteger shieldHP;
	private PositiveInteger hullHP;
	private PositiveInteger powergridOutput;
	private PositiveInteger capacitorAmount;
	private PositiveInteger capacitorRechargeRate;
	private PositiveInteger speed;
	private PositiveInteger size;
	private AttackSubsystem attacker;
	private DefenciveSubsystem defender;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
						   PositiveInteger powergridOutput, PositiveInteger capacitorAmount,
						   PositiveInteger capacitorRechargeRate, PositiveInteger speed,
						   PositiveInteger size, AttackSubsystem attacker, DefenciveSubsystem defender) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.attacker = attacker;
		this.defender = defender;
	}


	@Override
	public void endTurn() {
		// TODO: Ваш код здесь :)

	}

	@Override
	public void startTurn() {
		// TODO: Ваш код здесь :)

	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.size;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		// TODO: Ваш код здесь :)
		return null;
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		// TODO: Ваш код здесь :)
		return null;
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		// TODO: Ваш код здесь :)
		return null;
	}

}
