package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private final String name;
	private final PositiveInteger impactReduct;
	private final PositiveInteger shieldRegen;
	private final PositiveInteger hullRegen;
	private final PositiveInteger capacitorUsage;
	private final PositiveInteger pgRequirement;

	public DefenciveSubsystemImpl(String name, PositiveInteger impactReduction, PositiveInteger shieldRegen,
								  PositiveInteger hullRegen, PositiveInteger capacitorUsage,
								  PositiveInteger pgRequirement) {
		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		this.name = name;
		this.impactReduct = impactReduction;
		this.shieldRegen = shieldRegen;
		this.hullRegen = hullRegen;
		this.capacitorUsage = capacitorUsage;
		this.pgRequirement = pgRequirement;
	}

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
												   PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
												   PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		return new DefenciveSubsystemImpl(name,impactReductionPercent,shieldRegeneration,
				hullRegeneration, capacitorConsumption, powergridConsumption);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorUsage;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		int reductionPercents = Math.min(this.impactReduct.value(), 95);

		double impactReduction = (double) reductionPercents / 100;
		double reducedDamage = (double) incomingDamage.damage.value() * (1.0 - impactReduction);
		double round = Math.ceil(reducedDamage);

		return new AttackAction(PositiveInteger.of((int) round),
				incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegen, this.hullRegen);
	}

}
