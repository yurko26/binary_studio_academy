package com.binary_studio.academy_coin;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class AcademyCoin {

    private AcademyCoin() {
    }

    public static int maxProfit(Stream<Integer> prices) {
        int result = 0;
        int currentCoins = 0;
        List<Integer> list = prices.collect(Collectors.toList());
        for (int i = 0; i < list.size(); i++) {
            int max = list.get(i);
            int min = list.get(i);
            for (int j = i; j < list.size(); j++) {
                if (max < list.get(j)) {
                    max = list.get(j);
                }
                if (min > list.get(i)) {
                    min = list.get(i);
                }
            }
            if (currentCoins == 1) {
                int count = i;
                for (int j = i; j < list.size()-1; j++) {
                    if(list.get(j)<list.get(j+1)){
                        count++;
                    }
                    else{
                        break;
                    }
                }
                i = count;
                result += list.get(i);
                currentCoins = 0;
            } else if (list.get(i) == max) {
                continue;
            } else if (list.get(i) == min) {
                result -= list.get(i);
                currentCoins = 1;
            }
        }
        return result;
    }
}
